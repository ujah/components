import React from 'react'
import CommentDetail from './components/CommentDetail'
import ApprovalCard from './ApprovalCard'


const App = () => {
  return( 
    <div className="ui container comments">
    <ApprovalCard/>

    <CommentDetail author="sam"/>
    <CommentDetail author="mike"/>
    <CommentDetail author="jane"/>
    

    </div>
  );
};
 
export default App;
